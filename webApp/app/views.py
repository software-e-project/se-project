from flask import redirect, render_template, redirect, session, flash, request
from .forms import createAnAccount, loginUser, completeForm, paymentForm
from app import db, models, app
from .queries import check_bike_availability_out, check_bike_availability_in, check_bike_availability_out_object, check_bike_availability_in_object
from .db_population import make_booking, add_location
from werkzeug.security import generate_password_hash, check_password_hash
import json, math, re, itertools
import datetime
from datetime import timedelta, date
from flask_mail import Message, Mail
import qrcode

#functions called in app.route



def price_calculation(start, end):
    #possibly put into database to be edited by manager later
    difference = end - start
    difference_seconds = difference.total_seconds()
    minutes = divmod(difference_seconds, 60)[0]
    discountprice = 0
    if minutes >= 4320: #equal to 3 days
        discount = True
        slots = minutes / 15
        price = round((slots * 0.5), 2)
        discountprice = round((slots * 0.45), 2)
    else:
        discount = False;
        slots = minutes / 15
        price = round((slots * 0.5), 2)
        discountprice = 0
    return price, discountprice

def process_payment(card_number, CVV, expiry_date): #ensures credit card details match format
    card_pattern = '(?:\d{4}-?){3}\d{4}'
    CVV_pattern = '([0-9]){3}'
    expiry_pattern = '([0-9]){2}\/([0-9]){2}'
    if not re.match(card_pattern, card_number):
        return False
    if not re.match(CVV_pattern, CVV):
        return False
    if not re.match(expiry_pattern, expiry_date):
        return False
    return True


def add_user(username, password, email):
    password_hash = generate_password_hash(password, method='pbkdf2:sha256', salt_length=6)
    user = models.User(username=username, email=email, password=password_hash)
    db.session.add(user)
    db.session.commit()

def username_exists(username):
    if models.User.query.filter(models.User.username == username).count()>0:
        return 2 #indicates username already exists
    else:
        return 0

def email_exists(email): #cchecks user eexists
    if models.User.query.filter(models.User.email == email).count()>0:
        return 1 #indicates username already existsflasdk
    else:
        return 0

def login(username, password):
    for users in models.User.query.filter(models.User.username == username):
        password_hash = generate_password_hash("admin", method='pbkdf2:sha256', salt_length=6)
        if (check_password_hash(users.password, password)) == True:
            session["id"] = users.id
            return True

def register_user(username, email, password):
    password_hash = generate_password_hash(password, method='pbkdf2:sha256', salt_length=6)
    user = models.User(username=username, email=email, password=password_hash)
    db.session.add(user)
    db.session.commit()

#route returns

@app.route('/bookingDetails', methods=['GET', 'POST'])  #readys data for booking
def bookingDetails():
    data = json.loads(request.data.decode('utf-8'))
    datetimeout = (datetime.datetime(int(data.get('yearout')), int(data.get('monthout')), int(data.get('dayout')), int(data.get('hourout')), int(data.get('minuteout')), 00))
    locationout = data.get('locationout')
    datetimein = (datetime.datetime(int(data.get('yearin')), int(data.get('monthin')), int(data.get('dayin')), int(data.get('hourin')), int(data.get('minutein')), 00))
    locationin = data.get('locationin')

    #pushes booking data to seession for booking after payment
    session['datetimeout'] = datetimeout
    session['datetimein'] = datetimein
    session['locationout'] = locationout
    session['locationin'] = locationin
    return json.dumps("success")

@app.route('/availablePickup', methods=['GET', 'POST']) #shows available pick up locations based on time
def availablePickup():
    data = json.loads(request.data.decode('utf-8'))
    availableLocations = check_bike_availability_out_object(datetime.datetime(int(data.get('year')), int(data.get('month')), int(data.get('day')), int(data.get('hour')), int(data.get('minute')), 00))
    returnData = {}
    returnData['availableLocations'] = availableLocations
    return json.dumps(returnData)

@app.route('/availableDropoff', methods=['GET', 'POST']) #shows available drop offlocations based on time
def availableDropoff():
    data = json.loads(request.data.decode('utf-8'))
    availableLocations = check_bike_availability_in(datetime.datetime(int(data.get('year')), int(data.get('month')), int(data.get('day')), int(data.get('hour')), int(data.get('minute')), 00))
    returnData = {}
    returnData['availableLocations'] = availableLocations
    return json.dumps(returnData)

@app.route('/getLocations', methods=['GET', 'POST'])  #cgets avaialble locations now
def getLocations():
    ids = []
    names = []
    capacity = []
    lats = []
    longs = []
    locations = check_bike_availability_out_object(datetime.datetime.now())
    for location in locations:
        ids.append(location.id)
        names.append(location.location_name)
        capacity.append(30) #ADD QUERY
        lats.append(location.lat)
        longs.append(location.long)
    returnData = {}
    returnData['ids'] = ids
    returnData['availableLocations'] = names
    returnData['lats'] = lats
    returnData['longs'] = longs
    returnData['bikesAvailable'] = capacity
    return json.dumps(returnData)

@app.route('/getLocationsBooking', methods=['GET', 'POST'])  #
def getLocationsBooking(): #change to initial
    ids = []
    names = []
    capacity = []
    lats = []
    longs = []
    data = json.loads(request.data.decode('utf-8'))
    locations = check_bike_availability_out_object(datetime.datetime(int(data.get('year')), int(data.get('month')), int(data.get('day')), int(data.get('hour')), int(data.get('minute')), 00))
    for location in locations:
        ids.append(location.id)
        names.append(location.location_name)
        capacity.append(30)  # ADD QUERY
        lats.append(location.lat)
        longs.append(location.long)
    returnData = {}
    returnData['ids'] = ids
    returnData['availableLocations'] = names
    returnData['lats'] = lats
    returnData['longs'] = longs
    returnData['bikesAvailable'] = capacity
    return json.dumps(returnData)
    #CLEAN UP

@app.route('/getLocationsBookingIn', methods=['GET', 'POST'])  #
def getLocationsBookingIn():
    ids = []
    names = []
    capacity = []
    lats = []
    longs = []
    data = json.loads(request.data.decode('utf-8'))
    locations = check_bike_availability_in_object(
        datetime.datetime(int(data.get('year')), int(data.get('month')), int(data.get('day')),
                          int(data.get('hour')), int(data.get('minute')), 00))
    for location in locations:
        ids.append(location.id)
        names.append(location.location_name)
        capacity.append(30)  # ADD QUERY
        lats.append(location.lat)
        longs.append(location.long)
    returnData = {}
    returnData['ids'] = ids
    returnData['availableLocations'] = names
    returnData['lats'] = lats
    returnData['longs'] = longs
    returnData['bikesAvailable'] = capacity
    return json.dumps(returnData)

@app.route('/validate', methods=['GET', 'POST'])
def validate(): #checks if username or passwrod already in use
    data = json.loads(request.data.decode('utf-8'))
    exists = 0; #0 means doesnt exist, 1 means email already exists, 2 means username already exists
    username = str(data.get('username'))
    email = str(data.get('email'))
    exists = username_exists(username)
    if exists == 0:
        exists = email_exists(email)
    return json.dumps(exists)


@app.route("/bookingsData.json/<string:startingDate>/<string:endingDate>")
def returnBookingsData(startingDate, endingDate):
    # Convert strings to datetime.
    startingDates = startingDate.split("_")
    startingDateFormat = datetime.datetime(int(startingDates[0]), int(startingDates[1]), int(startingDates[2]))

    endingDates = endingDate.split("_")
    endingDateFormat = datetime.datetime(int(endingDates[0]), int(endingDates[1]), int(endingDates[2]))

    # Get data from database.
    bookings = []
    locationNames = []
    numberOfBookings = []
    numberOfLocations = 0
    for booking in models.Booking.query.all():
        bookings.append(booking)
    for location in models.Location.query.all():
        locationNames.append(location.location_name)
        numberOfLocations += 1
        numberOfBookings.append(0)
    for booking in bookings:
        # If bookings.pickup_time is greater than startingDate and less than endingDate.
        if (booking.pickup_time > startingDateFormat) and (booking.dropoff_time < endingDateFormat):
            i = 0
            while i < numberOfLocations:
                if booking.pickup_location == i:
                    numberOfBookings[i] += 1
                i = i + 1

    # Create json object.
    data = {}
    data['numberOfLocations'] = numberOfLocations
    data['numberOfBookings'] = numberOfBookings
    data['locations'] = locationNames
    returnObject = json.dumps(data)

    return returnObject

@app.route("/incomeData.json/<string:startingDate>")
def returnIncomeData(startingDate):
    # Convert startingDate to datetime.
    startingDates = startingDate.split("_")
    startingDateFormat = datetime.datetime(int(startingDates[0]), int(startingDates[1]), int(startingDates[2]))
    endingDateFormat = datetime.datetime(int(startingDates[0]), int(startingDates[1]), int(startingDates[2]))
    # Set ending date to the following sunday of the week.
    endingDateFormat = endingDateFormat + timedelta(days=6)

    # Get data from database.
    bookings = []
    numberOfLocations = 0
    incomes = []
    locationNames = []

    # Put the list of location names into the array locationNames.
    for location in models.Location.query.all():
        locationNames.append(location.location_name)
        numberOfLocations += 1
        incomes.append(0)

    # Put list of totat incomes by location in the array called incomes.
    for booking in models.Booking.query.all():
        bookings.append(booking)
    for booking in bookings:
        if (booking.pickup_time > startingDateFormat) and (booking.dropoff_time < endingDateFormat):
            i = 0
            while i < numberOfLocations:
                if booking.pickup_location == i:
                    # Add transaction amount for this booking to incomes under the current location.
                    for transaction in models.Transaction.query.filter_by(models.Transaction.id == booking.transaction_id).all():
                        incomes[i] += transaction.cost

    # Package data in json and return to the client.
    data = {}
    data['numberOfLocations'] = numberOfLocations
    data['incomeData'] = incomes
    data['locations'] = locationNames
    returnObject = json.dumps(data)

    return returnObject

@app.route('/')
def index():
    if session.get('id'):
        return render_template('newBookingForm.html')
    else: #redirect login if no session variable
        return redirect("/Login")

@app.route('/Login', methods=['GET', 'POST'])
def loginPage():
    if session.get('id'):
        session.pop('id') #pop session variable for re login
    form = loginUser()
    form_two = completeForm()
    if form.validate_on_submit():
        if(login(form.username.data, form.password.data) == True):
            session['username'] = form.username.data
            return redirect("/")
    return render_template('loginPage.html', form=form, formtwo=form_two)


@app.route('/CreateAnAccount', methods=['GET', 'POST'])
def AccountPage():
    form = createAnAccount()
    if form.validate_on_submit():
        add_user(form.username.data, form.password.data, form.email.data)
        return redirect("/Login")
    return render_template('createAnAccount.html', title = 'Account', form = form)

class Booking:
    def __init__(self, bookingID, dateIn, dateOut):
        self.bookingID = bookingID
        self.dateIn = dateIn
        self.dateOut = dateOut

@app.route('/TransactionHistory', methods=['GET', 'POST'])
def TransactionHistory():
    pastBookings = []
    currentBookings = []
    currentDate = date.today()
    for booking in models.Booking.query.filter_by(user_id = session.get('id')).all():
        if booking.dropoff_time.date() > currentDate:
            # If booking is current or in the future:
            currentBookings.append(Booking(booking.id, booking.pickup_time, booking.dropoff_time))
        else:
            # If booking has past:
            pastBookings.append(Booking(booking.id, booking.pickup_time, booking.dropoff_time))
    return render_template('transactionHistory.html', title = 'User Account Page', pastBookings = pastBookings, currentBookings = currentBookings)


@app.route('/Payment', methods=['GET', 'POST'])
def Payment():
    form = paymentForm()
    print(form.errors)
    if not session.get('id'):
        return redirect('/Login')
    cost, discountcost = price_calculation(session.get('datetimeout'), session.get('datetimein'))
    dateIn = str(session.get('datetimein'))
    dateOut = str(session.get('datetimeout'))
    locationIn = models.Location.query.get(int(session.get('locationin')[1:2]))
    locationInString = locationIn.location_name
    locationOut = models.Location.query.get(int(session.get('locationout')[1:2]))
    locationOutString = locationOut.location_name
    # print('HERERERE')
    # if form.validate_on_submit():
    #     print("here 1")
    #     transaction = models.Transaction(card_number=form.card_number.data, expiry_date=form.expiry_date.data, security_number=form.cvv.data, cost=cost)
    #     session['transaction_id'] = transaction.id
    #     db.session.add(transaction)
    #     db.session.commit()
    #     return redirect('/Email')
    print(discountcost)
    print(cost)
    return render_template('paymentPage.html', form=form, cost=cost, discountprice=discountcost, dateIn=dateIn, dateOut=dateOut, locationIn=locationInString, locationOut=locationOutString)

@app.route('/Email', methods = ['GET', 'POST'])
def email():
    print("here 2")
    #make booking now payment confirmed
    cost, discountcost = price_calculation(session.get('datetimeout'), session.get('datetimein'))
    if discountcost == 0:
        transaction = models.Transaction(cost=cost)
    else:
        transaction = models.Transaction(cost=discountcost)
    db.session.add(transaction)
    db.session.commit()
    make_booking(session.get("id"), session.get('datetimeout'), session.get('datetimein'), session.get('locationout'), session.get('locationin'), transaction.id)
    user = models.User.query.get(session.get('id'))
    pickupTime = session.get('datetimein')
    dropoffTime = session.get('datetimeout')
    img = qrcode.make(session.get('id'))
    msg = Message(subject="Your Reciept",
        sender='ecobikesinleeds@gmail.com',
        recipients=[user.email],
        body="Your username: " + str(user.username) +  "\nTime of pickup: " + str(pickupTime) + "\nTime of dropoff: " + str(dropoffTime) + "\n Price: £" + str(cost) + "\n\n Many thanks! From the EcoBikesInLeeds team!\n\n For queries or information about bookings, please feel free to email us at ecobikesinleeds@gmail.com.\n\n\n" + str(img))

    mail = Mail(app)
    mail.send(msg)
    #pop booking data
    session.pop('datetimeout')
    session.pop('datetimein')
    session.pop('locationout')
    session.pop('locationin')
    return redirect('/')


@app.route('/addLocation', methods=['GET', 'POST'])
def addLocation():
    add_location("City Centre", 30, 53.801277, -1.548567)
    add_location("Train Station", 20, 53.7950, -1.5474)
    return("locations added")
