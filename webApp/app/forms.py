from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, TextAreaField, TextField
from wtforms.validators import DataRequired


class createAnAccount(Form):
    username = TextField('username', validators=[DataRequired()])
    email = TextField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class loginUser(Form):
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class completeForm(Form):
    complete = BooleanField()

class paymentForm(Form):
    card_number = TextField('card_number', validators=[DataRequired()])
    cvv = TextField('cvv', validators=[DataRequired()])
    expiry_date = TextField('expiry_date', validators=[DataRequired()])
