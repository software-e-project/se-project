from app import db, models, app
from sqlalchemy import and_
import datetime
def check_bike_availability_out(requested_date_time):
    available_locations = []
    check_count = 0
    default_count = 25 #add this to database in location model
    for location in models.Location.query.all():
        removed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= requested_date_time)).count() #the amount of bikes picked up from location before this time
        added = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= requested_date_time)).count()
        current_count = default_count - removed + added #current bike count at location
        needed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)).count()
        if (current_count - 1) >= needed:#if bike can be taken without further calculation
            available_locations.append(str(location.location_name))
        elif current_count > 0:# -1
            new_count = current_count - 1
            pick_count = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)).count()
            for bookings in models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)): #check the >= on queries for errors
                new_count += models.Booking.query.filter(and_(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= bookings.pickup_time), models.Booking.dropoff_time > requested_date_time)).count()
                if new_count >= 1: #if this pick up can still be satisfied
                    check_count += 1
            if check_count == pick_count:
                available_locations.append(str(location.location_name))
    return available_locations

def check_bike_availability_out_object(requested_date_time):
    available_locations = []
    check_count = 0
    default_count = 25 #add this to database in location model
    for location in models.Location.query.all():
        removed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= requested_date_time)).count() #the amount of bikes picked up from location before this time
        added = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= requested_date_time)).count()
        current_count = default_count - removed + added #current bike count at location
        needed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)).count()
        if (current_count - 1) >= needed:#if bike can be taken without further calculation
            available_locations.append(location)
        elif current_count > 0:# -1
            new_count = current_count - 1
            pick_count = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)).count()
            for bookings in models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time > requested_date_time)): #check the >= on queries for errors
                new_count += models.Booking.query.filter(and_(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= bookings.pickup_time), models.Booking.dropoff_time > requested_date_time)).count()
                if new_count >= 1: #if this pick up can still be satisfied
                    check_count += 1
            if check_count == pick_count:
                available_locations.append(location)
    return available_locations

def check_bike_availability_in(requested_date_time):
    available_locations = []
    check_count = 0
    default_count = 25 #add this to database in location model
    for location in models.Location.query.all():
        removed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= requested_date_time)).count() #the amount of bikes picked up from location before this time
        added = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= requested_date_time)).count()
        current_count = default_count - removed + added #current bike count at location
        arrivals = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)).count()
        if (current_count + 1 + arrivals) <= location.max_capacity:#if bike can be taken without further calculation
            available_locations.append(str(location.location_name))
        elif current_count + 1 <= location.max_capacity:
            new_count = current_count + 1
            drop_count = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)).count()
            for bookings in models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)): #check the >= on queries for errors
                new_count -= models.Booking.query.filter(and_(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= bookings.pickup_time), models.Booking.pickup_time > requested_date_time)).count()
                if new_count <= location.max_capacity: #if this pick up can still be satisfied
                    check_count += 1
            if check_count == arrivals:
                available_locations.append(str(location.location_name))
    print(available_locations)
    return available_locations

def check_bike_availability_in_object(requested_date_time):
    available_locations = []
    check_count = 0
    default_count = 25 #add this to database in location model
    for location in models.Location.query.all():
        removed = models.Booking.query.filter(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= requested_date_time)).count() #the amount of bikes picked up from location before this time
        added = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time <= requested_date_time)).count()
        current_count = default_count - removed + added #current bike count at location
        arrivals = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)).count()
        if (current_count + 1 + arrivals) <= location.max_capacity:#if bike can be taken without further calculation
            available_locations.append(location)
        elif current_count + 1 <= location.max_capacity:
            new_count = current_count + 1
            drop_count = models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)).count()
            for bookings in models.Booking.query.filter(and_(models.Booking.dropoff_location == location.id, models.Booking.dropoff_time > requested_date_time)): #check the >= on queries for errors
                new_count -= models.Booking.query.filter(and_(and_(models.Booking.pickup_location == location.id, models.Booking.pickup_time <= bookings.pickup_time), models.Booking.pickup_time > requested_date_time)).count()
                if new_count <= location.max_capacity: #if this pick up can still be satisfied
                    check_count += 1
            if check_count == arrivals:
                available_locations.append(location)
    #print(available_locations)
    return available_locations

@app.route('/check') #usef for testing
def check():
    availableLocations = check_bike_availability_in(datetime.datetime(2010, 2, 10, 11, 00, 00))
    print(availableLocations)
    return str(availableLocations)
