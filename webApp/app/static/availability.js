
function changeDropoff() //updates drop off markers
{
	//removes drop off markers on map
	dropoff_location = null;
	var selected_time = document.getElementById("dropoffTimeOptions").value;
	for (x = 0; x <dropoffMarkers.length; x++)
	{
		dropoffMarkers[x].setMap(null);
	}

	dropoffMarkers = [];

	//parse date/time data to send to server for query
	var selected_date = document.getElementById("dropoffDateInput").value;
	var split_date = selected_date.split("-");
	var split_time = selected_time.split(":");
	var year_string = JSON.stringify(split_date[0])
	var month_string = JSON.stringify(split_date[1])
	var day_string = JSON.stringify(split_date[2])
	var hour_string = JSON.stringify(split_time[0])
	var minute_string = JSON.stringify(split_time[1])

		$.ajax
	({
		type: 'POST',
		url: '/getLocationsBookingIn',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ year: split_date[0], month: split_date[1], day: split_date[2], hour: split_time[0], minute: split_time[1]}), //let server check if user exist already
		dataType: "json",
				success: function(response)
				{
					for (i = 0; i < response.availableLocations.length; i++)
					{
						//place new markers
						markThree({lat: response.lats[i], lng: response.longs[i]}, response.ids[i], response.availableLocations[i], response.bikesAvailable[i]);
					}
				},
		error: function(error)
		{
			(error);
		}
	});
}


function updateDateTime()
{
	//get the current date
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth();
  var day = now.getDate();

  var hour = now.getHours();
  var minute = now.getMinutes();

	while (minute % 15 != 0) //round down so steps go up in 15
	{
		minute--;
	}

  if (month < 10) //add leading 0's if neccesary
  {
    month = "0" + month;
  }

  if (day < 10) //add leading 0's if neccesary
  {
    day = "0" + day;
  }

  if (hour < 10) //add leading 0's if neccesary
  {
    hour = "0" + hour;
  }

  if (minute < 10) //add leading 0's if neccesary
  {
    minute = "0" + minute;
  }

  var parsedTime = hour + ":" + minute;
  var parsedDate = year + "-" + month + "-" + day;

  var pickup = document.getElementById("pickupDateInput");
  pickup.setAttribute("value", parsedDate);
  pickup.setAttribute("min", parsedDate); //disallow user from selecting pick up date before this

  var dropoff = document.getElementById("dropoffDateInput");
  dropoff.setAttribute("value", parsedDate);
  dropoff.setAttribute("min", parsedDate); //disallow user from selecting pick up date before this

  var pickupTime = document.getElementById("pickupTimeOptions");
	(parsedTime);
  pickupTime.setAttribute("value", parsedTime);
  pickupTime.setAttribute("min", parsedTime);
}

function updateTimeSelection()
{
	//get selection elements
	var pickup = document.getElementById("pickupDateInput");
	var pickupTime = document.getElementById("pickupTimeOptions");
	var dropoff = document.getElementById("dropoffDateInput");
	var dropoffTime = document.getElementById("dropoffTimeOptions");

	//parse date/time
	var splitDropoff = dropoff.value.split("-");
	var splitPickup = pickup.value.split("-");
	var dropoffDate = new Date(splitDropoff[0], splitDropoff[1], splitDropoff[2]);
	var pickupDate = new Date(splitPickup[0], splitPickup[1], splitPickup[2]);

	if (dropoffDate.getTime() == pickupDate.getTime()) //if same day then the drop off time must be greater than the pickup time
	{
		dropoffTime.setAttribute("value", pickupTime.value);
		dropoffTime.setAttribute("min", pickupTime.value);
	}

	else //if the drop off day later than the pick up day then any dropoff time can be selected
	{
		dropoffTime.setAttribute("min", "null");
	}
}

function changePickup()
{
	//remove map markers
	pickup_location = null;
	var pickup_date = document.getElementById("pickupDateInput").value;
	var pickup_time = document.getElementById("pickupTimeOptions").value;
	for (x = 0; x <bookingMarkers.length; x++)
	{
		bookingMarkers[x].setMap(null);
	}

	var pickupText = document.getElementById("pickup");
	pickupText.innerHTML = "Pickup Location: ";
	bookingMarkers = [];

	//parse date/tim stuff
	var selected = document.getElementById("pickupTimeOptions");
	var selected_time = selected.value;
	var selected_date = document.getElementById("pickupDateInput").value;
	var split_date = selected_date.split("-");
	var split_time = selected_time.split(":");
	var year_string = JSON.stringify(split_date[0])
	var month_string = JSON.stringify(split_date[1])
	var day_string = JSON.stringify(split_date[2])
	var hour_string = JSON.stringify(split_time[0])

	$.ajax({
	type: 'POST',
	url: '/getLocationsBooking',
	contentType: "application/json; charset=utf-8",
	data: JSON.stringify({ year: split_date[0], month: split_date[1], day: split_date[2], hour: split_time[0], minute: split_time[1]}), //see if variable can be removed
	dataType: "json",
			success: function(response)
			{
				for (i = 0; i < response.availableLocations.length; i++)
				{
					//populate map with new marker
					markTwo({lat: response.lats[i], lng: response.longs[i]}, response.ids[i], response.availableLocations[i], response.bikesAvailable[i]);
				}
			},
			error: function(error)
			{
				console.log(error);
			}
	});
}

$(document).ready(function() {
	var alert = $("div.alert");
	alert.hide();
	updateDateTime(); //set date and time selection to current time

	// Set the CSRF token so that we are not rejected by server
	var csrf_token = $('meta[name=csrf-token]').attr('content');
	// Configure ajaxSetupso that the CSRF token is added to the header of every request
  $.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", csrf_token);
	        }
	    }
	});

	$("input.pickup-date").change(function()
	{
		var pickupTime = document.getElementById("pickupTimeOptions");
		pickupTime.setAttribute("min", "null"); //removes minimum time if date moved forward

	  var dropoffDate = document.getElementById("dropoffDateInput");
		var pickup_date = document.getElementById("pickupDateInput").value;

		dropoffDate.setAttribute("value", pickup_date);
		dropoffDate.setAttribute("min", pickup_date); //set minimum drop off date to pick up so user can't book for minus time
	  var dropoffTime = document.getElementById("dropoffTimeOptions");

		changePickup();
		updateTimeSelection(); //update drop off time
	});

	$("input.dropoff-date").change(function()
	{
		changeDropoff();
		updateTimeSelection();
	});

	$("input.pickup-time").change(function()
	{
		console.log("pick up time changed");
		changePickup();
		updateTimeSelection();

	});

	$("input.dropoff-time").change(function()
	{
		changeDropoff();
	});

	$("a.btn-send").click(function()  //send data to make booking
	{
		if (pickup_location == null || dropoff_location == null)
		{
			return;
		}

		//get date and time elements
		var pickup_date = document.getElementById("pickupDateInput").value;
		var pickup_time = document.getElementById("pickupTimeOptions").value;
		var dropoff_date = document.getElementById("dropoffDateInput").value;
		var dropoff_time = document.getElementById("dropoffTimeOptions").value;

		//parse date and time
		var split_date_out = pickup_date.split("-");
		var split_time_out = pickup_time.split(":");
		var year_string_out = JSON.stringify(split_date_out[0]);
		var month_string_out = JSON.stringify(split_date_out[1]);
		var day_string_out = JSON.stringify(split_date_out[2]);
		var hour_string_out = JSON.stringify(split_time_out[0]);
		var minute_string_out = JSON.stringify(split_time_out[1]);
		var split_date_in = dropoff_date.split("-");
		var split_time_in = dropoff_time.split(":");
		var year_string_in = JSON.stringify(split_date_in[0]);
		var month_string_in = JSON.stringify(split_date_in[1]);
		var day_string_in = JSON.stringify(split_date_in[2]);
		var hour_string_in = JSON.stringify(split_time_in[0]);
		var minute_string_in = JSON.stringify(split_time_in[1]);

		var pickup_string = JSON.stringify(pickup_location);
		var dropoff_string = JSON.stringify(dropoff_location);

		$.ajax(
		{
		type: 'POST',
		url: '/bookingDetails',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ yearout: split_date_out[0], monthout: split_date_out[1], dayout: split_date_out[2], hourout: split_time_out[0], minuteout: split_time_out[1],  locationout: pickup_string, yearin: split_date_in[0], monthin: split_date_in[1], dayin: split_date_in[2], hourin: split_time_in[0], minutein: split_time_in[1], locationin: dropoff_string}), //let server check if user exist already
		dataType: "json",
				success: function(response)
				{
					console.log("Recieved");
					 window.location.href = '/Payment'; //send to payment screen
				},
				error: function(error)
				{
					console.log(error);
				}
		})
	});
});
