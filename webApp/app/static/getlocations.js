//hide booking button and second map
$('.collapsed').hide();
$('.collapsed-two').hide();
var map, mapTwo;
var bookingMarkers = [];
var dropoffMarkers = [];
var pickup_location = null;
var dropoff_location = null;

function updateTimeSelection()
{
	//get selection elements
	var pickup = document.getElementById("pickupDateInput");
	var pickupTime = document.getElementById("pickupTimeOptions");
	var dropoff = document.getElementById("dropoffDateInput");
	var dropoffTime = document.getElementById("dropoffTimeOptions");

	//parse date/time
	var splitDropoff = dropoff.value.split("-");
	var splitPickup = pickup.value.split("-");
	var dropoffDate = new Date(splitDropoff[0], splitDropoff[1], splitDropoff[2]);
	var pickupDate = new Date(splitPickup[0], splitPickup[1], splitPickup[2]);

	if (dropoffDate.getTime() == pickupDate.getTime()) //if same day then the drop off time must be greater than the pickup time
	{
		dropoffTime.setAttribute("value", pickupTime.value);
		dropoffTime.setAttribute("min", pickupTime.value);
	}

	else //if the drop off day later than the pick up day then any dropoff time can be selected
	{
		dropoffTime.setAttribute("min", "null");
	}
}

function initMap() //initialisee google maps
{
  var myLatLng = {lat: 53.808479, lng: -1.552792};
  map = new google.maps.Map(document.getElementById('map'),
  {
    zoom: 14,
    center: myLatLng,
    zoomControl: false,
    scaleControl: true
  });

	mapTwo = new google.maps.Map(document.getElementById('map-two'),
	{
		zoom: 14,
		center: myLatLng,
		zoomControl: false,
		scaleControl: true
	});

  mapThree = new google.maps.Map(document.getElementById('map-three'),
	{
		zoom: 14,
		center: myLatLng,
		zoomControl: false,
		scaleControl: true
	});
}


function mark(location, id, name, available) //create marker maps
{
	console.log(name);
  var marker = new google.maps.Marker(
		{
    	position: location,
    	map: map,
			title: name,
      icon: {
      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }
    });

	marker.metadata = {type: "marker", id: id};

	var contentString = '<div id="content">'+
             '<div id="siteNotice">'+
             '</div>'+
             '<p>' +
						 name +
						 '</p> Bikes Available: ' +
						 '<p>' +
						 available +
						 '</p>' +
						 '</div>';
	var infowindow = new google.maps.InfoWindow(
	{
    content: contentString
	});


	marker.addListener('click', function()
  {
    console.log("clicked");
    console.log(marker.metadata.id);
		infowindow.open(map, marker);

  });
}


function markTwo(location, id, name, available) //create marker maps for pickup
{
  var marker = new google.maps.Marker(
		{
    	position: location,
    	map: mapTwo,
			title: name,
			icon: {
      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }
    });

	marker.metadata = {type: "marker", id: id, name: name};
	bookingMarkers.push(marker);
	var contentString = '<div id="content">'+
             '<div id="siteNotice">'+
             '</div>'+
             '<p>' +
						 name +
						 '</p> Bikes Available: ' +
						 '<p>' +
						 available +
						 '</p>' +
						 '<a id="' + id + '"type="submit" class="btn btn-secondary btn-look book">Pick Location</a>' +
						 '</div>';
	var infowindow = new google.maps.InfoWindow(
  {
    content: contentString
	});

	marker.addListener('click', function()
  {
		infowindow.open(mapTwo, marker);

  });
}

function markThree(location, id, name, available) //create marker maps for dropoff
{
  var marker = new google.maps.Marker(
		{
    	position: location,
    	map: mapThree,
			title: name,
			icon: {
      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }
    });

	marker.metadata = {type: "marker", id: id, name: name};
	dropoffMarkers.push(marker);
	var contentString = '<div id="content">'+
             '<div id="siteNotice">'+
             '</div>'+
             '<p>' +
						 name +
						 '</p> Bikes Available: ' +
						 '<p>' +
						 available +
						 '</p>' +
						 '<a id="' + id + '"type="submit" class="btn btn-secondary btn-look drop">Pick Location</a>' +
						 '</div>';
	var infowindow = new google.maps.InfoWindow(
  {
    content: contentString
	});


	marker.addListener('click', function()
  {
		infowindow.open(mapThree, marker);

  });
}

$(document).ready(function()
{
	var alert = $("div.alert");
	alert.hide();

  $(document).on('click', "a.drop", function() //change marker colour on drop off map
  {

    for (i = 0; i < dropoffMarkers.length; i++) //deslect current markere
		{
			if (dropoffMarkers[i].metadata.id == dropoff_location)
			{
				dropoffMarkers[i].setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
			}
		}

    var button = $(this);
    $('.collapsed-two').show();
    dropoff_location = button.attr('id');

    for (x = 0; x < dropoffMarkers.length; x++)
		{
			if (dropoffMarkers[x].metadata.id == dropoff_location) //highlight selected marker
			{
		    dropoffMarkers[x].setIcon('http://maps.google.com/mapfiles/ms/icons/purple-dot.png');
			}
		}
  });

	$(document).on('click', "a.book", function() //change marker colour for pickup map
	{
    updateTimeSelection();
		for (i = 0; i < bookingMarkers.length; i++)
		{
			if (bookingMarkers[i].metadata.id == pickup_location)
			{
				bookingMarkers[i].setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
			}
		}

		var button = $(this);
		$('.collapsed').show();
		pickup_location = button.attr('id');
		console.log(pickup_location);

		for (x = 0; x < bookingMarkers.length; x++)
		{
			if (bookingMarkers[x].metadata.id == pickup_location)
			{
				var pickup = document.getElementById("pickup");
				pickup.innerHTML = "Pickup Location: " + bookingMarkers[x].metadata.name;
				bookingMarkers[x].setIcon('http://maps.google.com/mapfiles/ms/icons/purple-dot.png');
			}
		}
	});

	// Set the CSRF token so that we are not rejected by server
	var csrf_token = $('meta[name=csrf-token]').attr('content');
	// Configure ajaxSetupso that the CSRF token is added to the header of every request
  $.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", csrf_token);
	        }
	    }
	});

    $.ajax({
    type: 'GET',
		url: '/getLocations',
    contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ test: 1}), 
    dataType: "json",
        success: function(response)
				{
					for (i = 0; i < response.availableLocations.length; i++)
					{
						mark({lat: response.lats[i], lng: response.longs[i]}, response.ids[i], response.availableLocations[i], response.bikesAvailable[i]);
						markTwo({lat: response.lats[i], lng: response.longs[i]}, response.ids[i], response.availableLocations[i], response.bikesAvailable[i]);
					}
		    },
				error: function(error)
				{
					console.log(error);
				}
		});
});
