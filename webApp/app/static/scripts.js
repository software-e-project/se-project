function tabOpen(evt, tabName) {
  var i, tabcontent, tablinks;

  tabcontent = document.getElementsByClassName("tabcontent");
  console.log("here");
  showArrow();
  setTimeout(function(){ var popup = document.getElementById("myPopup");popup.classList.toggle("hide"); }, 2000);

  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  tablinks = document.getElementsByClassName("card-img-top");

  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";

  var shownElement = document.getElementById(tabName);
  shownElement.scrollIntoView({behaviour: "smooth"})
}

function showArrow() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
