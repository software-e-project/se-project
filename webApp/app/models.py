from app import db

class User(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    username = db.Column(db.String(20), unique=True)
    email = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(100))

class Location(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    location_name = db.Column(db.String(20), unique=True)
    max_capacity = db.Column(db.Integer)
    lat = db.Column(db.Float)
    long = db.Column(db.Float)

class Bike(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'))
    delete = db.Column(db.Float)

class Transaction(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    # card_number = db.Column(db.String(20))
    # expiry_date = db.Column(db.String(20))
    # security_number = db.Column(db.String(20))
    cost = db.Column(db.Float)

class Booking(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('bike.id'))
    pickup_time = db.Column(db.DateTime)
    dropoff_time = db.Column(db.DateTime)
    pickup_location = db.Column(db.Integer, db.ForeignKey('location.id'))
    dropoff_location = db.Column(db.Integer, db.ForeignKey('location.id'))
    transaction_id = db.Column(db.Integer, db.ForeignKey('transaction.id'))
