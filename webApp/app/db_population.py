from flask import redirect, render_template, redirect, session, flash, request
from .forms import createAnAccount, loginUser, completeForm
from app import db, models, app
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import and_
import json
import datetime

def add_user(username, password, email):
    password_hash = generate_password_hash(password, method='pbkdf2:sha256', salt_length=6)
    user = models.User(username=username, email=email, password=password_hash)
    db.session.add(user)
    db.session.commit()


def add_location(location_name, max_capacity, lat, long):
    location = models.Location(location_name=location_name, max_capacity=max_capacity, lat=lat, long=long)
    db.session.add(location)
    db.session.commit()

def add_bike(location_name):
    for locations in models.Location.query.filter(models.Location.location_name == location_name):
        location_id = locations.id
    bike = models.Bike(location_id=location_id)
    db.session.add(bike)
    db.session.commit()

def add_transaction(card_number, expiry_date, security_number, cost):
    transaction = models.Transaction(card_number=card_number, expiry_date=expiry_date, security_number=security_number, cost=cost)
    db.session.add(transaction)
    db.session.commit()


def make_booking(user_id,pickup_time, dropoff_time, pickup_id, dropoff_id, transaction_id):
#assuming the booking is made after the transaction is confirmed
    booking = models.Booking(user_id=user_id,
    pickup_time=pickup_time, dropoff_time = dropoff_time, pickup_location = pickup_id, dropoff_location=dropoff_id, transaction_id=transaction_id)
    db.session.add(booking)
    db.session.commit()


@app.route('/addlocs')
def add_locs():
    #add_user("awfullyhot", "admin", "awfullyhot@admin.com")
    #add_location("University of Leeds", 30, 53.808479, -1.552792)
    #add_location("Millenium Square", 30, 53.800755, -1.549077)
    return "Locations Added"

@app.route('/testQuery')
def test_query():
    make_booking(1, datetime.datetime.now(), datetime.datetime.now(), 1, 2)
    print(datetime.datetime.now())
    #make_booking(1, datetime.datetime(2011, 2, 10, 10, 30, 00), datetime.datetime(2011, 2, 10, 11, 00, 00) , "Burley Train station", "University Union")
    return "booking Made"

@app.route('/printBookings')
def print_bookings():
    for booking in models.Booking.query.all():
        print("id: ", booking.booking_id, "user id: ", booking.user_id , "pick up time: ", booking.pickup_time, "drop off time: ", booking.dropoff_time, "pick up location: ",  booking.pickup_location, "drop off location: ", booking.dropoff_location)
    return "booking printed"
