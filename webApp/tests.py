import unittest
from app import db, models, app
from werkzeug.security import generate_password_hash, check_password_hash


# ////////////////////////////////////////////////////////////////////////////////
# testing the adding of users to database
def add_user(user, email, password):
    password_hash = generate_password_hash(password, method='pbkdf2:sha256', salt_length=6)
    testuser = models.User(username=user, email=email, password=password_hash)
    db.session.add(testuser)
    return True

class Testadduser(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        db.session.rollback()
        pass

    def test_add_user(self):
        assert add_user('testuser', 'testuser@gmail.com', 'testpassword') == True


# ////////////////////////////////////////////////////////////////////////////////
def username_exists(username):
    password_hash = generate_password_hash("testpassword", method='pbkdf2:sha256', salt_length=6)
    testuser = models.User(username="testuser", email="test@gmail.com", password=password_hash)
    db.session.add(testuser)
    if models.User.query.filter(models.User.username == username).count()>0:
        return 2 #indicates username already exists
    else:
        return 0

class testuserexitst(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        db.session.rollback()
        pass

    def test_user_true(self):
        assert username_exists('testuser') == True

    def test_user_false(self):
        assert username_exists('nottestuser') == False



# ////////////////////////////////////////////////////////////////////////////////
def email_exists(email):
    password_hash = generate_password_hash("testpassword", method='pbkdf2:sha256', salt_length=6)
    testuser = models.User(username="testuser", email="test@gmail.com", password=password_hash)
    db.session.add(testuser)
    if models.User.query.filter(models.User.email == email).count()>0:
        return 1 #indicates username already existsflasdk
    else:
        return 0

class testemailexists(unittest.TestCase):
        def setUp(self):
            pass

        def tearDown(self):
            db.session.rollback()
            pass

        def test_email_true(self):
            assert username_exists('test@gmail.com') == True

        def test_email_false(self):
            assert username_exists('nottest@gmail.com') == False

# ////////////////////////////////////////////////////////////////////////////////
# testing of the login system
def login(username, password):
    # password_hash = generate_password_hash('testpassword', method='pbkdf2:sha256', salt_length=6)
    # testuser = models.User(username='testuser', email='testuser@gmail.com', password= 'testpassword')
    # db.session.add(testuser)
    add_user("testuser", "test@gmail.com", "testpassword")
        for users in models.User.query.filter(models.User.username == username):
        password_hash = generate_password_hash("admin", method='pbkdf2:sha256', salt_length=6)
        if (check_password_hash(users.password, password)) == True:
            session["id"] = users.id
            return True
    return False

class Testlogin(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        db.session.rollback()
        pass

    def test_login(self):
        assert login('testuser', 'testpassword') == True

    def test_fail_case(self):
        assert login('wronguser', 'testpassword') == True


# ////////////////////////////////////////////////////////////////////////////////
# def pricecalc (start, end): ADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD



# ////////////////////////////////////////////////////////////////////////////////
def process_payment(card_number, CVV, expiry_date):
    card_pattern = '(?:\d{4}-?){3}\d{4}'
    CVV_pattern = '([0-9]){3}'
    expiry_pattern = '([0-9]){2}\/([0-9]){2}'
    if not re.match(card_pattern, card_number):
        return False
    if not re.match(CVV_pattern, CVV):
        return False
    if not re.match(expiry_pattern, expiry_date):
        return False
    return True

# CHECK CARD NO STUCTURE AND ALL THE STUCTURES ACTUALLY correct
class Testlogin(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        db.session.rollback()
        pass

    def test_card_no_fail(self):
        assert process_payment('(1234)(1234)', '(123)', '(12)(20)') == False

    def test_cvv_fail(self):
        assert process_payment('(1234)(1234)(1234)(1234)', '(1)', '(12)(20)') == False

    def test_date_fail(self):
        assert process_payment('(1234)(1234)(1234)(1234)', '(1)', '(20)') == False

    def test_all_correct(self):
        assert process_payment('(1234)(1234)(1234)(1234)', '(123)', '(12)(20)') == True

# /////////////////////////////////////////////////////////////////////////////////
if __name__ == '__main__':
    unittest.main()
