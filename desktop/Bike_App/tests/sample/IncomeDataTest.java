package sample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class IncomeDataTest {
    IncomeData testIncomeObject;

    @BeforeEach
    void setUp() {
        ArrayList<Double> incomeData = new ArrayList<>();
        incomeData.add(1.1);
        incomeData.add(2.2);
        incomeData.add(3.3);
        ArrayList<String> locations = new ArrayList<>();
        locations.add("Location1");
        locations.add("Location2");
        locations.add("Location3");
        int numberOfLocations = 3;
        testIncomeObject = new IncomeData(numberOfLocations, locations, incomeData);
    }

    @Test
    void getNumberOfLocations() {
        assertEquals(3, testIncomeObject.getNumberOfLocations());
    }

    @Test
    void getLocations() {
        assertEquals("Location1", testIncomeObject.getLocations().get(0));
        assertEquals("Location2", testIncomeObject.getLocations().get(1));
        assertEquals("Location3", testIncomeObject.getLocations().get(2));
    }

    @Test
    void getIncomeList() {
        assertEquals(1.1, testIncomeObject.getIncomeList().get(0).doubleValue());
        assertEquals(2.2, testIncomeObject.getIncomeList().get(1).doubleValue());
        assertEquals(3.3, testIncomeObject.getIncomeList().get(2).doubleValue());

    }
}