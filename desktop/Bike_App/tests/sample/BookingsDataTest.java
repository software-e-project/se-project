package sample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BookingsDataTest {
    BookingsData testBookingsObject;

    @BeforeEach
    void setUp() {
        ArrayList<Integer> numberOfBookings = new ArrayList<>();
        numberOfBookings.add(1);
        numberOfBookings.add(2);
        numberOfBookings.add(3);
        ArrayList<String> locations = new ArrayList<>();
        locations.add("Location1");
        locations.add("Location2");
        locations.add("Location3");
        int numberOfLocations = 3;
        testBookingsObject = new BookingsData(numberOfLocations, numberOfBookings, locations);
    }

    @Test
    void getNumberOfLocations() {
        assertEquals(3, testBookingsObject.getNumberOfLocations());
    }

    @Test
    void getNumberOfBookings() {
        assertEquals(1, testBookingsObject.getNumberOfBookings(0));
        assertEquals(2, testBookingsObject.getNumberOfBookings(1));
        assertEquals(3, testBookingsObject.getNumberOfBookings(2));
    }

    @Test
    void getLocationName() {
        assertEquals("Location1", testBookingsObject.getLocationName(0));
        assertEquals("Location2", testBookingsObject.getLocationName(1));
        assertEquals("Location3", testBookingsObject.getLocationName(2));
    }
}