package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.util.ArrayList;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("CompareLocations.fxml"));
        root.getStylesheets().add(getClass().getResource("styling.css").toExternalForm());



        // Set application title to 'Bike Hire Admin' / Sets Applications Size / Makes the Applications un-resizable.

        primaryStage.setTitle("Bike Hire Admin");
        // When doing the demo, change locations to root after showing the graph
        primaryStage.setScene(new Scene(root, 1276, 772));
        primaryStage.setResizable(false);

        //Show the primary window.
        primaryStage.show();
    }

    // Runs the main class and launches the application.
    public static void main(String[] args) {
        launch(args);
    }
}


