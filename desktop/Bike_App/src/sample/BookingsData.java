package sample;

import java.util.ArrayList;

class BookingsData {
  private int numberOfLocations;
  private ArrayList<Integer> numberOfBookings;
  private ArrayList<String> locations;

  public BookingsData(int numberOfLocations, ArrayList<Integer> numberOfBookings, ArrayList<String> locations) {
    this.numberOfLocations = numberOfLocations;
    this.numberOfBookings = numberOfBookings;
    this.locations = locations;
  }

  public int getNumberOfLocations() { return numberOfLocations; }

  public int getNumberOfBookings(int indexOfLocation) {
    return numberOfBookings.get(indexOfLocation);
  }
  public String getLocationName(int indexOfLocation) {
    return locations.get(indexOfLocation);
  }
}
