package sample;

import java.util.ArrayList;

class IncomeData {
  private int numberOfLocations;
  private ArrayList<String> locations;
  private ArrayList<Double> incomeData;

  public IncomeData(int numberOfLocations, ArrayList<String> locations, ArrayList<Double> incomeData) {
    this.numberOfLocations = numberOfLocations;
    this.locations = locations;
    this.incomeData = incomeData;
  }

  public int getNumberOfLocations() { return numberOfLocations; }

  public ArrayList<String> getLocations() { return locations; }

  public ArrayList<Double> getIncomeList() { return incomeData; }
}
