package sample;

import java.net.*;
import java.io.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

class ServerConnection {

  private String url;

  public ServerConnection(String url) {
    this.url = url;
  }

  public BookingsData getBookingsData(String startingDate, String endingDate) {
    StringBuilder stringBuilder = new StringBuilder();

    // Work out parameters for url.
    url += "/";
    url += startingDate;
    url += "/";
    url += endingDate;

    try {
      // Establish connection.
      URL urlObject = new URL(url);
      HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
      connection.setRequestMethod("GET");
      connection.setReadTimeout(15000);
      connection.setInstanceFollowRedirects(true);
      HttpURLConnection.setFollowRedirects(true);
      connection.connect();

      BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      // Get data as a string.
      String line = null;
      while ((line = reader.readLine()) != null)
      {
        stringBuilder.append(line);
      }

    } catch (Exception e) {
      System.out.println("There was an exception: " + e.toString());
    }

    // Parse the string as  a json object using the Gson library.
    Gson gson = new GsonBuilder().create();
    System.out.println(stringBuilder.toString());
    BookingsData returnObject = gson.fromJson(stringBuilder.toString(), BookingsData.class);

    return returnObject;
  }

  public IncomeData getIncomeData(String startingDate) {
    StringBuilder stringBuilder = new StringBuilder();

    // Work out url using input date.
    url += "/";
    url += startingDate;

    try {
      // Establish connection.
      URL urlObject = new URL(url);
      HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
      connection.setRequestMethod("GET");
      connection.setReadTimeout(15000);
      connection.setInstanceFollowRedirects(true);
      HttpURLConnection.setFollowRedirects(true);
      connection.connect();

      BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      // Get data as a string.
      String line = null;
      while ((line = reader.readLine()) != null)
      {
        stringBuilder.append(line);
      }

    } catch (Exception e) {
      System.out.println("There was an exception: " + e.toString());
    }

    // Parse the string as  a json object using the Gson library.
    Gson gson = new GsonBuilder().create();
    System.out.println(stringBuilder.toString());
    IncomeData returnObject = gson.fromJson(stringBuilder.toString(), IncomeData.class);

    return returnObject;
  }
}
