package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.StackPane;

import java.awt.*;
import java.awt.print.Book;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.*;
import java.net.URL;

public  class Controller implements Initializable {


    // Navigation to switch between pages.

    @FXML
    protected StackPane rootStackPane;

    @FXML
    protected StackPane parentStackPane;

    @FXML
    protected void loadIncome() throws IOException
    {
        StackPane pane = FXMLLoader.load(getClass().getResource("Income.fxml"));
        rootStackPane.getChildren().setAll(pane);
    }

    @FXML
    protected void loadCompare() throws IOException
    {
        StackPane pane = FXMLLoader.load(getClass().getResource("CompareLocations.fxml"));
        parentStackPane.getChildren().setAll(pane);
    }


    // Bar Chart information




    // Income

    @FXML
    protected BarChart<?,?> IncomeBarChart;

    @FXML
    protected CategoryAxis x2;

    @FXML
    protected NumberAxis y2;

    // Compare Locations
    @FXML
    protected BarChart<?,?>  CompareBarChart;


    @FXML
    protected CategoryAxis x;

    @FXML
    protected NumberAxis y;



    // Date Pickers

    // Income

    @FXML
    protected DatePicker week;


    // Compare Locations
    @FXML
    protected DatePicker datePick1;

    @FXML
    protected DatePicker datePick2;


    // chooseDate() gets the 2 dates from the date pickers, formats them as a string and then uses the string to get the URL.
    @FXML
    protected BookingsData chooseDate()
    {

        String firstDate = datePick1.getValue().toString();
        String secondDate = datePick2.getValue().toString();

        if (datePick2.getValue().isBefore(datePick1.getValue())) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Please choose a start date before the end date.");
            alert.showAndWait();
        }


        // Replaces dates - with underscore in order to make URL correct i.e 12-01-2019 to 12_01_2019
        String inputFirstDate = firstDate.replace("-", "_");
        String inputSecondDate = secondDate.replace("-", "_");

        ServerConnection connection = new ServerConnection("https://SoftEngProjBoys.pythonanywhere.com/bookingsData.json");
        BookingsData BookingObject = connection.getBookingsData(inputFirstDate, inputSecondDate);

        return BookingObject;
    }



    // When the done button is pressed this functions runs.
    // On the button action if the server is connected, data is added into the graph on click.
    @FXML
    protected void doneButtonPressed()
    {
        try {
            BookingsData data = chooseDate();

            XYChart.Series Compare = new XYChart.Series<>();
            CompareBarChart.setLegendVisible(false);
            CompareBarChart.getData().clear();


            // Gets every location name and number of bookings and adds it to a list.

            // Adds columns to graph using the arrays.
            int i = 0;
            while (i < data.getNumberOfLocations()) {
                Compare.getData().add(new XYChart.Data(data.getLocationName(i), data.getNumberOfBookings(i)));
                ++i;
            }

            CompareBarChart.getData().addAll(Compare);


        } catch (Exception e) {
            // Display a message if the program cant connect to the server.
            Alert alert = new Alert(Alert.AlertType.ERROR, "Received error when communicating with server:\n" + e.toString());
            alert.showAndWait();
            System.out.println(e.toString());
        }

    }


    // Income data graph

    // Same as ChooseDate() but for Income data instead.
    @FXML
    protected IncomeData Income()
    {

        IncomeData incomeObject = null;
        String chooseWeek;
        week.setShowWeekNumbers(true);

        // If Date picked is not monday set monday to first day of the week.
        if(week.getValue().getDayOfWeek() != DayOfWeek.MONDAY)
        {
            LocalDate currentDate = week.getValue(); //
            week.setValue(currentDate.with(DayOfWeek.MONDAY));
        }

            chooseWeek = week.getValue().toString();
            String getWeek = chooseWeek.replace("-", "_");

            // Get the income data for the week commencing on getWeek.
            ServerConnection connection = new ServerConnection("https://SoftEngProjBoys.pythonanywhere.com/incomeData.json");
            incomeObject = connection.getIncomeData(getWeek);


        return incomeObject;

    }

    // Same as doneButtonClicked but for income.
    @FXML
    protected void doneButtonClicked2()
    {
        try {

            IncomeData data = Income();


            XYChart.Series IncomeDataGraph = new XYChart.Series<>();
            IncomeBarChart.getData().clear();

            int i = 0;
            double totalIncome = 0;
            while (i < data.getNumberOfLocations()) {
                IncomeDataGraph.getData().add(new XYChart.Data(data.getLocations().get(i), data.getIncomeList().get(i)));
                totalIncome += data.getIncomeList().get(i);
                ++i;
            }
            IncomeDataGraph.getData().add(new XYChart.Data("Total Income", totalIncome));
            IncomeBarChart.getData().addAll(IncomeDataGraph);
        }
        catch(Exception e)
        {
            System.out.println(e.toString());

            // Display a message if the program cant connect to the server.
            Alert alert = new Alert(Alert.AlertType.ERROR, "Received error when communicating with server:\n" + e.toString());
            alert.showAndWait();

        }
    }

//    // Same as doneButtonClicked but for income.
//    @FXML
//    protected void doneButtonClicked2()
//    {
//        try {
//
//            IncomeData data = Income();
//            ArrayList<String> daysOfWeek = new ArrayList<>();
//            daysOfWeek.add("Monday");
//            daysOfWeek.add("Tuesday");
//            daysOfWeek.add("Wednesday");
//            daysOfWeek.add("Thursday");
//            daysOfWeek.add("Friday");
//            daysOfWeek.add("Saturday");
//            daysOfWeek.add("Sunday");
//
//            XYChart.Series IncomeDataGraph = new XYChart.Series<>();
//            IncomeBarChart.getData().clear();
//
//            int i = 0;
//            while (i < 7) {
//                IncomeDataGraph.getData().add(new XYChart.Data(daysOfWeek.get(i), data.getIncomeList().get(i)));
//                ++i;
//            }
//            IncomeBarChart.getData().addAll(IncomeDataGraph);
//        }
//        catch(Exception e)
//        {
//            System.out.println(e.toString());
//
//            // Display a message if the program cant connect to the server.
//            Alert alert = new Alert(Alert.AlertType.ERROR, "Received error when communicating with server:\n" + e.toString());
//            alert.showAndWait();
//
//        }
//    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}



