import java.net.*;
import java.io.*;

class workingConnection {
  public static void main(String ags[]) {
    System.out.println("Hello World!");

    try {
      URL url = new URL("http://127.0.0.1:5000/bookingsData.json");
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      connection.setReadTimeout(10000);
      connection.connect();

      BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      StringBuilder stringBuilder = new StringBuilder();

      String line = null;
      while ((line = reader.readLine()) != null)
      {
        stringBuilder.append(line + "\n");
      }
      System.out.println(stringBuilder.toString());

    } catch (Exception e) {
      System.out.println("There was an exception: " + e.toString());
    }
  }
}
